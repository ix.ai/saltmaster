#!/usr/bin/env sh

set -e

chmod -R 700 /etc/salt/gpgkeys
chown -R salt:salt /etc/salt/gpgkeys

if [ -f /etc/salt/gpgkeys/pubring.kbx ]; then
  echo "GPG key ring already exists!"
else
  echo "WARNING: GPG Keyring not found. Generating..."
  cat >/SALTSTACK <<EOF
  %echo Generating a basic OpenPGP key
  %no-protection
  Key-Type: RSA
  Key-Length: 4096
  Subkey-Type: RSA
  Subkey-Length: 4096
  Name-Real: Salt Master in Docker
  Name-Email: salt-master@docker
  Expire-Date: 0
  # Do a commit here, so that we can later print "done" :-)
  %commit
  %echo done
EOF
  su - salt -c 'gpg --batch --homedir /etc/salt/gpgkeys --generate-key /SALTSTACK'
  rm /SALTSTACK
fi

su - salt -c 'gpg --list-secret-keys --homedir /etc/salt/gpgkeys'
su - salt -c 'gpg --armor --export --homedir /etc/salt/gpgkeys'
