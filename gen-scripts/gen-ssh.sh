#!/usr/bin/env sh

set -eu

chmod -R 700 /etc/salt/sshkeys
chown -R salt:salt /etc/salt/sshkeys

if [ -f /etc/salt/sshkeys/saltmaster ]; then
  chmod 600 /etc/salt/sshkeys/saltmaster*
  echo "SSH public key already exists!"
else
  echo "WARNING: SSH Key not found. Generating..."
  su - salt -c "ssh-keygen -q -t ed25519 -N '' -C 'salt-master@docker' -f /etc/salt/sshkeys/saltmaster"
fi

echo "The current public key is:"
cat /etc/salt/sshkeys/saltmaster.pub
