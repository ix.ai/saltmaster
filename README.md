# saltmaster

[![Pipeline Status](https://gitlab.com/ix.ai/saltmaster/badges/master/pipeline.svg)](https://gitlab.com/ix.ai/saltmaster/)
[![Docker Image Size (latest)](https://img.shields.io/docker/image-size/ixdotai/saltmaster/latest)](https://hub.docker.com/r/ixdotai/saltmaster/)
[![Gitlab Project](https://img.shields.io/badge/GitLab-Project-554488.svg)](https://gitlab.com/ix.ai/saltmaster/)

This image started as an extenstion of the (now archived) project [/saltstack/open/saltdocker](https://github.com/saltstack/open/saltdocker).

## Added / Upgraded

* Built-in support for [gitfs](https://docs.saltstack.com/en/latest/topics/tutorials/gitfs.html) using [GitPython](https://github.com/gitpython-developers/GitPython)
* Build-in support for [gpg](https://docs.saltstack.com/en/latest/ref/renderers/all/salt.renderers.gpg.html)
* Always installs the latest [pypi salt](https://pypi.org/project/salt/) version currently available

## Usage

* Use as you normally would [saltstack/salt:latest](https://hub.docker.com/r/saltstack/salt/)
* Mount your SSH private key under `/etc/salt/sshkeys/saltmaster`
* Mount your GPG homedir under `/etc/salt/gpgkeys`

**Note**: If you don't supply the SSH private key or the GPG homedir, they will be automatically generated

### Optionally create the keys manually

SSH Keys:

```sh
docker volume create sshkeys
docker run --rm --volume sshkeys:/etc/salt/sshkeys registry.gitlab.com/ix.ai/saltmaster gen-ssh.sh
```

GPG Keyring:

```sh
docker volume create gpgkeys
docker run --rm --volume gpgkeys:/etc/salt/gpgkeys registry.gitlab.com/ix.ai/saltmaster gen-gpg.sh
```

## Start it up

```sh
docker run --rm \
           --volume sshkeys:/etc/salt/sshkeys \
           --volume gpgkeys:/etc/salt/gpgkeys \
           --volume cache:/var/cache/salt \
           --volume pki:/etc/salt/pki \
           -p 4505:4505 \
           -p 4506:4506 \
           registry.gitlab.com/ix.ai/saltmaster:latest
```

## Environment Variables

Inherited from the [original project](https://gitlab.com/saltstack/open/saltdocker), the Salt image uses several environment variables which are easy to miss. While none of the variables are required, they may significantly aid you in using the image.

### `SALT_MASTER_CONFIG`

A JSON object. This variable is dumped to /etc/salt/master.d/master.conf and can be used to provide extra config for the salt master.

### `SALT_API_CONFIG`

A JSON object. This variable is dumped to /etc/salt/master.d/api.conf, and defaults to the following.

```yaml
rest_cherrypy:
  port: 8000,
  ssl_crt: /etc/pki/tls/certs/localhost.crt
  ssl_key: /etc/pki/tls/certs/localhost.key
external_auth:
    sharedsecret:
        salt: ['.*', '@wheel', '@jobs', '@runner']
sharedsecret: $SALT_SHARED_SECRET
```

### `SALT_SHARED_SECRET`

If this environment variable is set, it will set the sharedsecret variable for using the salt-api with the salt user.

## Version tags

* `registry.gitlab.com/ix.ai/saltmaster:dev-master` is the latest build on the `master` branch
* `registry.gitlab.com/ix.ai/saltmaster:latest` is the latest build on a git tag
* all other tags follow the salt releases

**Warning**: The build on the master branch always installs the **latest** [pypi salt](https://pypi.org/project/salt/) version at the time of the pipeline run!

## Multi-Arch Support

Starting with `3005.1`, multi-arch support was enabled for this image. This was disabled again in `3007.1` and currently only AMD64 is supported.

## Resources

* GitLab: <https://gitlab.com/ix.ai/saltmaster>
* GitLab Registry: <https://gitlab.com/ix.ai/saltmaster/container_registry>
* Docker Hub: <https://hub.docker.com/r/ixdotai/saltmaster>
