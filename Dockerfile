FROM public.ecr.aws/docker/library/alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c AS builder

ENV CRYPTOGRAPHY_DONT_BUILD_RUST="1"

# This file is partially inspired from
# https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.Dockerfile

COPY requirements.txt /work/requirements.txt

RUN set -xeu; \
    mkdir -p /work/wheels; \
    apk add \
      cargo \
      cmake \
      g++ \
      gcc \
      libffi-dev \
      libgit2-dev \
      musl-dev \
      make \
      py3-cryptography \
      py3-pip \
      py3-psutil \
      py3-pyzmq \
      py3-wheel \
      python3-dev \
      openssl-dev \
    ;

RUN set -xeu; \
    time pip3 wheel --prefer-binary -w /work/wheels -r /work/requirements.txt

FROM public.ecr.aws/docker/library/alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

LABEL maintainer="docker@ix.ai" \
      ai.ix.repository="ix.ai/saltmaster"

COPY --from=builder /work /

RUN set -exu; \
    ls -lashi /wheels; \
    apk add --no-cache \
      gnupg \
      git \
      openssh-client \
      py3-pip \
      bash \
    ; \
    pip3 install \
      --no-index \
      --no-cache-dir \
      --find-links /wheels \
      --break-system-packages \
      -r /requirements.txt \
    ; \
    rm -rf /wheels /requirements.txt; \
    addgroup -g 450 -S salt; \
    adduser -s /bin/sh -SD -G salt salt; \
    mkdir -p \
      /home/salt/.ssh/ \
      /etc/salt/gpgkeys/ \
      /etc/salt/sshkeys/ \
      /etc/pki \
      /etc/salt/pki \
      /etc/salt/minion.d/ \
      /etc/salt/master.d \
      /etc/salt/proxy.d \
      /var/cache/salt \
      /var/log/salt \
      /var/run/salt \
    ; \
    chmod -R 2775 \
      /etc/pki \
      /etc/salt \
      /var/cache/salt \
      /var/log/salt \
      /var/run/salt \
    ; \
    chown salt:salt \
      /etc/salt/gpgkeys \
      /etc/salt/sshkeys \
      /var/log/salt/ \
      /var/run/salt/ \
      /var/cache/salt \
      /etc/salt/pki/ \
      /home/salt/.ssh/ \
      /etc/pki/ \
    ; \
    chmod -R 700 \
      /etc/salt/gpgkeys \
      /etc/salt/pki \
      /etc/salt/sshkeys \
    ; \
    su - salt -c 'salt-run salt.cmd tls.create_self_signed_cert'

ADD entrypoint.sh /
ADD --chown=salt:salt ssh-config /home/salt/.ssh/config
ADD gen-scripts/* /usr/local/bin/
ADD saltinit.py /usr/local/bin/saltinit

VOLUME ["/etc/salt/pki", "/var/cache/salt", "/etc/salt/gpgkeys", "/etc/salt/sshkeys", "/etc/salt/master.d"]

CMD ["/entrypoint.sh", "/usr/local/bin/saltinit"]

EXPOSE 4505 4506 8000
